package tk.strakar.bankingflow.base.entities;

public class Compte {

    private int id;
    private String nom;
    private int type_compte;
    private int utilisateur;

    public Compte(int id, String nom, int type_compte, int utilisateur) {
        this.id = id;
        this.nom = nom;
        this.type_compte = type_compte;
        this.utilisateur = utilisateur;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getType_compte() {
        return type_compte;
    }

    public void setType_compte(int type_compte) {
        this.type_compte = type_compte;
    }

    public int getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(int utilisateur) {
        this.utilisateur = utilisateur;
    }
}
