package tk.strakar.bankingflow.base.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import tk.strakar.bankingflow.base.GestionBdd;
import tk.strakar.bankingflow.base.entities.Role;

public class RoleManager {
    private static final String TABLE_NAME = "role";
    public static final String KEY_ID_ROLE = "id_" + TABLE_NAME;
    public static final String KEY_NOM_ROLE = "nom_" + TABLE_NAME;
    public static final String CREATE_TABLE_ROLE = "CREATE TABLE " + TABLE_NAME + " (" +
            KEY_ID_ROLE + " INTEGER primary key not null unique, " +
            KEY_NOM_ROLE + " TEXT) ";

    private GestionBdd maBaseSQLite; // notre gestionnaire du fichier SQLite
    private SQLiteDatabase db;

    // Constructeur
    public RoleManager(Context context)
    {
        maBaseSQLite = GestionBdd.getInstance(context);
    }

    public void open()
    {
        //on ouvre la table en lecture/écriture
        db = maBaseSQLite.getWritableDatabase();
    }

    public void close()
    {
        //on ferme l'accès à la BDD
        db.close();
    }

    public long addRole(Role role) {
        // Ajout d'un enregistrement dans la table

        ContentValues values = new ContentValues();
        values.put(KEY_NOM_ROLE, role.getLibelle());

        // insert() retourne l'id du nouvel enregistrement inséré, ou -1 en cas d'erreur
        return db.insert(
                TABLE_NAME,null,values);
    }

    public int modRole(Role role) {
        // modification d'un enregistrement
        // valeur de retour : (int) nombre de lignes affectées par la requête

        ContentValues values = new ContentValues();
        values.put(KEY_NOM_ROLE, role.getLibelle());

        String where = KEY_ID_ROLE+" = ?";
        String[] whereArgs = {role.getId()+""};

        return db.update(TABLE_NAME, values, where, whereArgs);
    }

    public int supRole(Role role) {
        // suppression d'un enregistrement
        // valeur de retour : (int) nombre de lignes affectées par la clause WHERE, 0 sinon

        String where = KEY_ID_ROLE+" = ?";
        String[] whereArgs = {role.getId()+""};

        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public Role getRole(int id) {
        // Retourne l'role dont l'id est passé en paramètre

        Role a = new Role(0,"");

        Cursor c = db.rawQuery(
                "SELECT * FROM "+TABLE_NAME+" WHERE "+
                        KEY_ID_ROLE+"="+id, null);
        if (c.moveToFirst())
        {
            a.setId(c.getInt(c.getColumnIndex(KEY_ID_ROLE)));
            a.setLibelle(c.getString(c.getColumnIndex(KEY_NOM_ROLE)));
            c.close();
        }

        return a;
    }

    public Cursor getRoles() {
        // sélection de tous les enregistrements de la table
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }

}
