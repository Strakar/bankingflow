package tk.strakar.bankingflow.base.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import tk.strakar.bankingflow.base.GestionBdd;
import tk.strakar.bankingflow.base.entities.Operation;

public class OperationManager {
    private static final String TABLE_NAME = "operation";
    public static final String KEY_ID_OPERATION = "id_" + TABLE_NAME;
    public static final String KEY_COMPTE_DESTINATION_OPERATION = "compte_dest_" + TABLE_NAME;
    public static final String KEY_MONTANT_OPERATION = "montant_" + TABLE_NAME;
    public static final String KEY_ETAT_COMPTE_MENSUEL_OPERATION = "etat_compte_mensuel_" + TABLE_NAME;
    public static final String KEY_DATE_OPERATION = "date_" + TABLE_NAME;

    public static final String CREATE_TABLE_OPERATION = "CREATE TABLE " + TABLE_NAME + " (" +
            KEY_ID_OPERATION + " INTEGER primary key not null unique, " +
            KEY_COMPTE_DESTINATION_OPERATION + " INTEGER REFERENCES compte (id_compte) ON DELETE SET DEFAULT, " +
            KEY_MONTANT_OPERATION + " TEXT, " +
            KEY_ETAT_COMPTE_MENSUEL_OPERATION + " INTEGER REFERENCES etat_compte_mensuel (id_etat_compte_mensuel) ON DELETE RESTRICT, " +
            KEY_DATE_OPERATION + " TEXT );";

    private GestionBdd maBaseSQLite; // notre gestionnaire du fichier SQLite
    private SQLiteDatabase db;

    // Constructeur
    public OperationManager(Context context) {
        maBaseSQLite = GestionBdd.getInstance(context);
    }

    public void open() {
        //on ouvre la table en lecture/écriture
        db = maBaseSQLite.getWritableDatabase();
    }

    public void close() {
        //on ferme l'accès à la BDD
        db.close();
    }

    public long addOperation(Operation operation) {
        // Ajout d'un enregistrement dans la table

        ContentValues values = new ContentValues();
        values.put(KEY_COMPTE_DESTINATION_OPERATION, operation.getCompteDestination());
        values.put(KEY_DATE_OPERATION, operation.getDate());
        values.put(KEY_ETAT_COMPTE_MENSUEL_OPERATION, operation.getEtatCompteMensuel());
        values.put(KEY_MONTANT_OPERATION, operation.getMontant());

        // insert() retourne l'id du nouvel enregistrement inséré, ou -1 en cas d'erreur
        return db.insert(
                TABLE_NAME, null, values);
    }

    public int modOperation(Operation operation) {
        // modification d'un enregistrement
        // valeur de retour : (int) nombre de lignes affectées par la requête

        ContentValues values = new ContentValues();
        values.put(KEY_COMPTE_DESTINATION_OPERATION, operation.getCompteDestination());
        values.put(KEY_DATE_OPERATION, operation.getDate());
        values.put(KEY_ETAT_COMPTE_MENSUEL_OPERATION, operation.getEtatCompteMensuel());
        values.put(KEY_MONTANT_OPERATION, operation.getMontant());

        String where = KEY_ID_OPERATION + " = ?";
        String[] whereArgs = {operation.getId() + ""};

        return db.update(TABLE_NAME, values, where, whereArgs);
    }

    public int supOperation(Operation operation) {
        // suppression d'un enregistrement
        // valeur de retour : (int) nombre de lignes affectées par la clause WHERE, 0 sinon

        String where = KEY_ID_OPERATION + " = ?";
        String[] whereArgs = {operation.getId() + ""};

        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public Operation getOperation(int id) {
        // Retourne l'operation dont l'id est passé en paramètre

        Operation a = new Operation(0, 0, 0, 0, "");

        Cursor c = db.rawQuery(
                "SELECT * FROM " + TABLE_NAME + " WHERE " +
                        KEY_ID_OPERATION + "=" + id, null);
        if (c.moveToFirst()) {
            a.setId(c.getInt(c.getColumnIndex(KEY_ID_OPERATION)));
            a.setCompteDestination(c.getInt(c.getColumnIndex(KEY_COMPTE_DESTINATION_OPERATION)));
            a.setDate(c.getString(c.getColumnIndex(KEY_DATE_OPERATION)));
            a.setEtatCompteMensuel(c.getInt(c.getColumnIndex(KEY_ETAT_COMPTE_MENSUEL_OPERATION)));
            a.setMontant(c.getInt(c.getColumnIndex(KEY_MONTANT_OPERATION)));

            c.close();
        }

        return a;
    }

    public Cursor getOperations() {
        // sélection de tous les enregistrements de la table
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }
}
