package tk.strakar.bankingflow.base.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import tk.strakar.bankingflow.base.GestionBdd;
import tk.strakar.bankingflow.base.entities.Compte;

public class CompteManager {
    private static final String TABLE_NAME = "compte";
    public static final String KEY_ID_COMPTE = "id_" + TABLE_NAME;
    public static final String KEY_NOM_COMPTE = "nom_" + TABLE_NAME;
    public static final String KEY_TYPE_COMPTE_COMPTE = "type_compte_" + TABLE_NAME;
    public static final String KEY_UTILISATEUR_COMPTE = "utilisateur_" + TABLE_NAME;


    public static final String CREATE_TABLE_COMPTE = "CREATE TABLE " + TABLE_NAME + " (" +
            KEY_ID_COMPTE + " INTEGER primary key not null unique, " +
            KEY_NOM_COMPTE + " TEXT, " +
            KEY_TYPE_COMPTE_COMPTE + " INTEGER REFERENCES compte (id_compte) ON DELETE RESTRICT," +
            KEY_UTILISATEUR_COMPTE + " INTEGER REFERENCES utilisateur (id_utilisateur) ON DELETE RESTRICT);";

    private GestionBdd maBaseSQLite; // notre gestionnaire du fichier SQLite
    private SQLiteDatabase db;

    // Constructeur
    public CompteManager(Context context) {
        maBaseSQLite = GestionBdd.getInstance(context);
    }

    public void open() {
        //on ouvre la table en lecture/écriture
        db = maBaseSQLite.getWritableDatabase();
    }

    public void close() {
        //on ferme l'accès à la BDD
        db.close();
    }

    public long addCompte(Compte compte) {
        // Ajout d'un enregistrement dans la table

        ContentValues values = new ContentValues();
        values.put(KEY_NOM_COMPTE, compte.getNom());
        values.put(KEY_TYPE_COMPTE_COMPTE, compte.getType_compte());
        values.put(KEY_UTILISATEUR_COMPTE, compte.getUtilisateur());

        // insert() retourne l'id du nouvel enregistrement inséré, ou -1 en cas d'erreur
        return db.insert(
                TABLE_NAME, null, values);
    }

    public int modCompte(Compte compte) {
        // modification d'un enregistrement
        // valeur de retour : (int) nombre de lignes affectées par la requête

        ContentValues values = new ContentValues();
        values.put(KEY_NOM_COMPTE, compte.getNom());
        values.put(KEY_TYPE_COMPTE_COMPTE, compte.getType_compte());
        values.put(KEY_UTILISATEUR_COMPTE, compte.getUtilisateur());
        String where = KEY_ID_COMPTE + " = ?";
        String[] whereArgs = {compte.getId() + ""};

        return db.update(TABLE_NAME, values, where, whereArgs);
    }

    public int supCompte(Compte compte) {
        // suppression d'un enregistrement
        // valeur de retour : (int) nombre de lignes affectées par la clause WHERE, 0 sinon

        String where = KEY_ID_COMPTE + " = ?";
        String[] whereArgs = {compte.getId() + ""};

        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public Compte getCompte(int id) {
        // Retourne l'compte dont l'id est passé en paramètre

        Compte a = new Compte(0, "", 0, 0);

        Cursor c = db.rawQuery(
                "SELECT * FROM " + TABLE_NAME + " WHERE " +
                        KEY_ID_COMPTE + "=" + id, null);
        if (c.moveToFirst()) {
            a.setId(c.getInt(c.getColumnIndex(KEY_ID_COMPTE)));
            a.setNom(c.getString(c.getColumnIndex(KEY_NOM_COMPTE)));
            a.setType_compte(c.getInt(c.getColumnIndex(KEY_TYPE_COMPTE_COMPTE)));
            a.setUtilisateur(c.getInt(c.getColumnIndex(KEY_UTILISATEUR_COMPTE)));
            c.close();
        }

        return a;
    }

    public Cursor getComptes() {
        // sélection de tous les enregistrements de la table
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }
}
