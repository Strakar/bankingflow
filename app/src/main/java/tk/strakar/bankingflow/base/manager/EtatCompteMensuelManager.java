package tk.strakar.bankingflow.base.manager;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.ParseException;

import tk.strakar.bankingflow.base.GestionBdd;
import tk.strakar.bankingflow.base.entities.EtatCompteMensuel;

public class EtatCompteMensuelManager {
    private static final String TABLE_NAME = "etat_compte_mensuel";
    public static final String KEY_ID_ETAT_COMPTE_MENSUEL = "id_" + TABLE_NAME;
    public static final String KEY_COMPTE_ETAT_COMPTE_MENSUEL = "compte_" + TABLE_NAME;
    public static final String KEY_DATE_ETAT_COMPTE_MENSUEL = "date_" + TABLE_NAME;
    public static final String KEY_SOLDE_DEBUT_ETAT_COMPTE_MENSUEL = "solde_debut_" + TABLE_NAME;
    public static final String KEY_SOLDE_FIN_ETAT_COMPTE_MENSUEL = "solde_fin_" + TABLE_NAME;

    public static final String CREATE_TABLE_ETAT_COMPTE_MENSUEL = "CREATE TABLE " + TABLE_NAME + " (" +
            KEY_ID_ETAT_COMPTE_MENSUEL + " INTEGER primary key not null unique, " +
            KEY_DATE_ETAT_COMPTE_MENSUEL + " DATE, " +
            KEY_SOLDE_DEBUT_ETAT_COMPTE_MENSUEL + " DOUBLE, " +
            KEY_SOLDE_FIN_ETAT_COMPTE_MENSUEL + " DOUBLE, " +
            KEY_COMPTE_ETAT_COMPTE_MENSUEL + " INTEGER REFERENCES compte(id_compte));";

    private GestionBdd maBaseSQLite; // notre gestionnaire du fichier SQLite
    private SQLiteDatabase db;

    // Constructeur
    public EtatCompteMensuelManager(Context context) {
        maBaseSQLite = GestionBdd.getInstance(context);
    }

    public void open() {
        //on ouvre la table en lecture/écriture
        db = maBaseSQLite.getWritableDatabase();
    }

    public void close() {
        //on ferme l'accès à la BDD
        db.close();
    }

    public EtatCompteMensuel getEtatCompteMensuel(int id) throws ParseException {
        // Retourne l'etat_compte_mensuel dont l'id est passé en paramètre

        EtatCompteMensuel a = new EtatCompteMensuel(0, 0, null, 0, 0);
        Cursor c = db.rawQuery(
                "SELECT * FROM " + TABLE_NAME + " WHERE " +
                        KEY_ID_ETAT_COMPTE_MENSUEL + "=" + id, null);
        if (c.moveToFirst()) {
            a.setId(c.getInt(c.getColumnIndex(KEY_ID_ETAT_COMPTE_MENSUEL)));
            a.setCompte(c.getInt(c.getColumnIndex(KEY_COMPTE_ETAT_COMPTE_MENSUEL)));
            a.setDate(c.getString(c.getColumnIndex(KEY_COMPTE_ETAT_COMPTE_MENSUEL)));
            a.setSoldeDebut(c.getInt(c.getColumnIndex(KEY_SOLDE_DEBUT_ETAT_COMPTE_MENSUEL)));
            a.setSoldeFin(c.getDouble(c.getColumnIndex(KEY_SOLDE_FIN_ETAT_COMPTE_MENSUEL)));
            c.close();
        }

        return a;
    }

    public Cursor getEtatCompteMensuels() {
        // sélection de tous les enregistrements de la table
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }
}
