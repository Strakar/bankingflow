package tk.strakar.bankingflow.base.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import tk.strakar.bankingflow.base.GestionBdd;
import tk.strakar.bankingflow.base.entities.Role;
import tk.strakar.bankingflow.base.entities.TypeCompte;
import tk.strakar.bankingflow.base.entities.Utilisateur;

public class TypeCompteManager {
    private static final String TABLE_NAME               = "type_compte";
    public static final  String KEY_ID_TYPE_COMPTE       = "id_"        + TABLE_NAME;
    public static final  String KEY_NOM_TYPE_COMPTE      = "nom_"       + TABLE_NAME;
    public static final  String KEY_TAUX_TYPE_COMPTE     = "taux_"      + TABLE_NAME;
    public static final  String CREATE_TABLE_TYPE_COMPTE = "CREATE TABLE " + TABLE_NAME+ " (" +
                                                            KEY_ID_TYPE_COMPTE       + " INTEGER primary key not null unique, " +
                                                            KEY_NOM_TYPE_COMPTE      + " TEXT, " +
                                                            KEY_TAUX_TYPE_COMPTE     + " DOUBLE);";


    private GestionBdd maBaseSQLite; // notre gestionnaire du fichier SQLite
    private SQLiteDatabase db;

    // Constructeur
    public TypeCompteManager(Context context)
    {
        maBaseSQLite = GestionBdd.getInstance(context);
    }

    public void open()
    {
        //on ouvre la table en lecture/écriture
        db = maBaseSQLite.getWritableDatabase();
    }

    public void close()
    {
        //on ferme l'accès à la BDD
        db.close();
    }

    public long addTypeCompte(TypeCompte typeCompte) {
        // Ajout d'un enregistrement dans la table

        ContentValues values = new ContentValues();
        values.put(KEY_NOM_TYPE_COMPTE, typeCompte.getNom());

        // insert() retourne l'id du nouvel enregistrement inséré, ou -1 en cas d'erreur
        return db.insert(
                TABLE_NAME,null,values);
    }

    public int modTypeCompte(TypeCompte typeCompte) {
        // modification d'un enregistrement
        // valeur de retour : (int) nombre de lignes affectées par la requête

        ContentValues values = new ContentValues();
        values.put(KEY_NOM_TYPE_COMPTE, typeCompte.getNom());

        String where = KEY_ID_TYPE_COMPTE+" = ?";
        String[] whereArgs = {typeCompte.getId()+""};

        return db.update(TABLE_NAME, values, where, whereArgs);
    }

    public int supTypeCompte(TypeCompte typeCompte) {
        // suppression d'un enregistrement
        // valeur de retour : (int) nombre de lignes affectées par la clause WHERE, 0 sinon

        String where = KEY_ID_TYPE_COMPTE+" = ?";
        String[] whereArgs = {typeCompte.getId()+""};

        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public TypeCompte getTypeCompte(int id) {
        // Retourne l'utilisateur dont l'id est passé en paramètre

        TypeCompte a = new TypeCompte(0,"",0);

        Cursor c = db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE " + KEY_ID_TYPE_COMPTE + "=" + id, null);
        if (c.moveToFirst())
        {
            a.setId(c.getInt(c.getColumnIndex(KEY_ID_TYPE_COMPTE)));
            a.setNom(c.getString(c.getColumnIndex(KEY_NOM_TYPE_COMPTE)));
            c.close();
        }

        return a;
    }

    public Cursor getTypeComptes() {
        // sélection de tous les enregistrements de la table
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }

}
