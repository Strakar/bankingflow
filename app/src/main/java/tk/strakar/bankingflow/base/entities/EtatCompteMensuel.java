package tk.strakar.bankingflow.base.entities;

public class EtatCompteMensuel {

    private int id;
    private int compte;
    private String date;
    private double soldeDebut;
    private double soldeFin;


    public EtatCompteMensuel(int id, int compte, String date, double soldeDebut, double soldeFin) {
        this.id = id;
        this.compte = compte;
        this.date = date;
        this.soldeDebut = soldeDebut;
        this.soldeFin = soldeFin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompte() {
        return compte;
    }

    public void setCompte(int compte) {
        this.compte = compte;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getSoldeDebut() {
        return soldeDebut;
    }

    public void setSoldeDebut(double soldeDebut) {
        this.soldeDebut = soldeDebut;
    }

    public double getSoldeFin() {
        return soldeFin;
    }

    public void setSoldeFin(double soldeFin) {
        this.soldeFin = soldeFin;
    }
}
