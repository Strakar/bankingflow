package tk.strakar.bankingflow.base;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import tk.strakar.bankingflow.base.manager.UtilisateurManager;

public class GestionBdd extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "db.sqlite";
    private static final int DATABASE_VERSION = 1;
    private static GestionBdd sInstance;

    public static synchronized GestionBdd getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new GestionBdd(context);
        }
        return sInstance;
    }

    private GestionBdd(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // Création de la base de données
        // on exécute ici les requêtes de création des tables
        //sqLiteDatabase.execSQL(RoleManager.CREATE_TABLE_ROLE);
        //sqLiteDatabase.execSQL(TypeCompteManager.CREATE_TABLE_TYPE_COMPTE);
        sqLiteDatabase.execSQL(UtilisateurManager.CREATE_TABLE_UTILISATEUR);
        //sqLiteDatabase.execSQL(CompteManager.CREATE_TABLE_COMPTE);
        //sqLiteDatabase.execSQL(EtatCompteMensuelManager.CREATE_TABLE_ETAT_COMPTE_MENSUEL);
        //sqLiteDatabase.execSQL(OperationManager.CREATE_TABLE_OPERATION);
    }

    @Override
    public void onUpgrade(
            SQLiteDatabase sqLiteDatabase, int i, int i2) {
        // Mise à jour de la base de données
        // méthode appelée sur incrémentation de DATABASE_VERSION
        // on peut faire ce qu'on veut ici, comme recréer la base :
        onCreate(sqLiteDatabase);
    }

}