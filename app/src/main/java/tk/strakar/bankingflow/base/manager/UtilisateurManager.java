package tk.strakar.bankingflow.base.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import tk.strakar.bankingflow.base.GestionBdd;
import tk.strakar.bankingflow.base.entities.Role;
import tk.strakar.bankingflow.base.entities.Utilisateur;

public class UtilisateurManager {
    private static final String TABLE_NAME               = "utilisateur";
    public static final  String KEY_ID_UTILISATEUR       = "id_"        + TABLE_NAME;
    public static final  String KEY_NOM_UTILISATEUR      = "nom_"       + TABLE_NAME;
    public static final  String KEY_PRENOM_UTILISATEUR   = "prenom_"    + TABLE_NAME;
    public static final  String KEY_MAIL_UTILISATEUR     = "mail_"      + TABLE_NAME;
    public static final  String KEY_PASSWORD_UTILISATEUR = "password_"  + TABLE_NAME;
    public static final  String KEY_ROLE_UTILISATEUR     = "role_"      + TABLE_NAME;
    public static final  String CREATE_TABLE_UTILISATEUR      = "CREATE TABLE "+TABLE_NAME+ " (" +
                                                            KEY_ID_UTILISATEUR       + " INTEGER primary key not null unique, " +
                                                            KEY_NOM_UTILISATEUR      + " TEXT, " +
                                                            KEY_PRENOM_UTILISATEUR   + " TEXT, " +
                                                            KEY_MAIL_UTILISATEUR     + " TEXT, " +
                                                            KEY_PASSWORD_UTILISATEUR + " TEXT, " +
                                                            KEY_ROLE_UTILISATEUR     + " INTEGER REFERENCES role (id_role) ON DELETE SET DEFAULT);";

    private GestionBdd maBaseSQLite; // notre gestionnaire du fichier SQLite
    private SQLiteDatabase db;

    // Constructeur
    public UtilisateurManager(Context context)
    {
        maBaseSQLite = GestionBdd.getInstance(context);
    }

    public void open()
    {
        //on ouvre la table en lecture/écriture
        db = maBaseSQLite.getWritableDatabase();
    }

    public void close()
    {
        //on ferme l'accès à la BDD
        db.close();
    }

    public long addUtilisateur(Utilisateur utilisateur) {
        // Ajout d'un enregistrement dans la table

        ContentValues values = new ContentValues();
        values.put(KEY_NOM_UTILISATEUR, utilisateur.getNom());

        // insert() retourne l'id du nouvel enregistrement inséré, ou -1 en cas d'erreur
        return db.insert(TABLE_NAME,null,values);
    }

    public int modUtilisateur(Utilisateur utilisateur) {
        // modification d'un enregistrement
        // valeur de retour : (int) nombre de lignes affectées par la requête

        ContentValues values = new ContentValues();
        values.put(KEY_NOM_UTILISATEUR, utilisateur.getNom());

        String where = KEY_ID_UTILISATEUR+" = ?";
        String[] whereArgs = {utilisateur.getId()+""};

        return db.update(TABLE_NAME, values, where, whereArgs);
    }

    public int supUtilisateur(Utilisateur utilisateur) {
        // suppression d'un enregistrement
        // valeur de retour : (int) nombre de lignes affectées par la clause WHERE, 0 sinon

        String where = KEY_ID_UTILISATEUR+" = ?";
        String[] whereArgs = {utilisateur.getId()+""};

        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public Utilisateur getUtilisateur(int id) {
        // Retourne l'utilisateur dont l'id est passé en paramètre

        Utilisateur a = new Utilisateur(0,null,null,null,null,0);

        Cursor c = db.rawQuery(
                "SELECT * FROM "+TABLE_NAME+" WHERE "+
                        KEY_ID_UTILISATEUR+"="+id, null);
        if (c.moveToFirst())
        {
            a.setId(c.getInt(c.getColumnIndex(KEY_ID_UTILISATEUR)));
            a.setNom(c.getString(c.getColumnIndex(KEY_NOM_UTILISATEUR)));
            c.close();
        }

        return a;
    }

    public Cursor getUtilisateurs() {
        // sélection de tous les enregistrements de la table
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }

}


