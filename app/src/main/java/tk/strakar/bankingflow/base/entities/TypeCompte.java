package tk.strakar.bankingflow.base.entities;

public class TypeCompte {

    private int id;
    private String nom;
    private double taux;

    public TypeCompte(int id, String nom, double taux) {
        this.id = id;
        this.nom = nom;
        this.taux = taux;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }
}
