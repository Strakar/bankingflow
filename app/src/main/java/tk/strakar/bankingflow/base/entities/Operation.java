package tk.strakar.bankingflow.base.entities;

public class Operation {

    private int id;
    private int compteDestination;
    private double montant;
    private int etatCompteMensuel;
    private String date;


    public Operation(int id, int compteDestination, double montant, int etatCompteMensuel, String date) {
        this.id = id;
        this.compteDestination = compteDestination;
        this.montant = montant;
        this.etatCompteMensuel = etatCompteMensuel;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompteDestination() {
        return compteDestination;
    }

    public void setCompteDestination(int compteDestination) {
        this.compteDestination = compteDestination;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public int getEtatCompteMensuel() {
        return etatCompteMensuel;
    }

    public void setEtatCompteMensuel(int etatCompteMensuel) {
        this.etatCompteMensuel = etatCompteMensuel;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
